<?php
ini_set("default_socket_timeout", -1);

use Yoye\Broker\Adapter\PhpRedisAdapter;

require_once __DIR__ . '/vendor/autoload.php';

// db
define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');

// redis
define('REDIS_HOST', 'localhost');
define('REDIS_PORT', 6379);
define('REDIS_TIMEOUT', 0);
define('FIBONACCI_QUEUE', 'fibonacci-queue');
define('PRIME_QUEUE', 'prime-queue');

// producer
define('COUNT', 'c');
define('TIMEOUT', 't');
define('OPTIONS', COUNT . ':' . TIMEOUT . ':');

$client = new Redis();
$client->connect(REDIS_HOST, REDIS_PORT, REDIS_TIMEOUT);
$adapter = new PhpRedisAdapter($client);
