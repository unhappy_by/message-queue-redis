<?php

use Yoye\Broker\Broker;

require __DIR__ . '/config.php';

$options = getopt(OPTIONS);

$broker = new Broker($adapter, []);

foreach (getPrimeSequenceGenerator((int) $options[COUNT]) as $prime) {
    $broker->queue($prime, PRIME_QUEUE);
    echo 'Prime number ' . $prime . ' added to queue.' . PHP_EOL;
    usleep((int) $options[TIMEOUT]);
}

function getPrimeSequenceGenerator(int $count): Generator {
    $number = 2;

    $i = 0;
    while ($i < $count) {
        $numberOfDivisions = 0;

        for ($j = 1; $j <= $number; $j++) {
            if ( ($number % $j) == 0) {
                $numberOfDivisions++;
            }
        }
        if ($numberOfDivisions < 3) {
            $i = $i + 1;
            yield (string) $number;
        }
        $number = $number + 1;
    }
}


