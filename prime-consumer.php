<?php

use Brick\Math\BigInteger;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Yoye\Broker\Broker;
use Yoye\Broker\Event\BrokerEvents;
use Yoye\Broker\Event\MessageEvent;

require_once __DIR__ . '/config.php';

$pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD);

$dispatcher = new EventDispatcher();
$dispatcher->addListener(BrokerEvents::MESSAGE_RECEIVED, function(MessageEvent $event) use ($pdo) {
    $pdo->beginTransaction();

    $number = BigInteger::of($event->getMessage());
    $row = $pdo->query('SELECT * FROM test FOR UPDATE')->fetch();

    $statement = $pdo->prepare('UPDATE test SET sum=:sum, count_prime=:count_prime');
    $statement->bindValue(':sum', $number->plus($row['sum']));
    $statement->bindValue(':count_prime', $row['count_prime']+1);
    $statement->execute();

    $event->setDone();
    $pdo->commit();
});

$broker = new Broker($adapter, [PRIME_QUEUE], $dispatcher);
$broker->run();
