# Подготовка и запуск

1. Подготовка базы
    ```
    CREATE TABLE test (`sum` TEXT, `count_fib` TEXT, `count_prime` TEXT) ENGINE=InnoDB CHARSET=utf8;

    INSERT INTO test VALUES (0,0,0);
    ```
1. Запуск консюмеров
    - `php ./fibonacci-consumer.php`
    - `php ./prime-consumer.php`
1. Запуск команд для генерации чисел
    - `php ./fibonacci-producer.php -c 2000 -t 1`
    - `php ./prime-producer.php -c 5000 -t 1`

Аргументы для продюсеров:<br>
    - `-c` - количество цифр для генерации<br>
    - `-t` - задержка в микросекундах<br>
