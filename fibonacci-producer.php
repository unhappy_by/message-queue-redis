<?php

use Brick\Math\BigInteger;
use Yoye\Broker\Broker;

require __DIR__ . '/config.php';

$options = getopt(OPTIONS);

$broker = new Broker($adapter, []);

foreach (getFibonacciSequenceGenerator((int) $options[COUNT]) as $fibonacci) {
    $broker->queue($fibonacci, FIBONACCI_QUEUE);
    echo 'Fibonacci number ' . $fibonacci . ' added to queue.' . PHP_EOL;
    usleep((int) $options[TIMEOUT]);
}

function getFibonacciSequenceGenerator(int $count): Generator {
    $prevNumber = BigInteger::of(0);
    $nextNumber = BigInteger::of(1);

    for($i = 0; $i < $count; $i++)
    {
        $result = $prevNumber->plus($nextNumber);
        $prevNumber = $nextNumber;
        $nextNumber = $result;

        yield (string) $result;
    }
}
